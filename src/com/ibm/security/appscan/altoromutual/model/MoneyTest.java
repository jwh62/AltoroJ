package com.ibm.security.appscan.altoromutual.model;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoneyTest {

    @Test
    public void testAccountBalanceGetter() {
        Money balance1 = new Money(5.4, "USD");
        Account account1 = new Account(1234, "myAccount", balance1);

        assertEquals(account1.getBalance(), new Money(5.4, "USD"));
    }

    @Test
    public void testAccountBalanceSum() {
        Money balance1 = new Money(5.4, "USD");
        Account account = new Account(1234, "myAccount", balance1);
        Money deposit = new Money(2.0, "USD");
        Expression sum = account.getBalance().plus(deposit);

        Bank bank= new Bank();
        Money newBalance = bank.reduce(sum, "USD");
        account.setBalance(newBalance);

        assertEquals(account.getBalance(), new Money(7.4, "USD"));
    }

}

